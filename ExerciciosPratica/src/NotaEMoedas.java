import java.util.Scanner;
public class NotaEMoedas {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		
		double N = sc.nextDouble();
		
		double resto = N;
		
		int nota = 100;
		int quosciente = (int) resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 50;
		quosciente = (int) resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 20;
		quosciente = (int) resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 10;
		quosciente = (int) resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 5;
		quosciente = (int) resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 2;
		quosciente = (int) resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		double moeda = 1.00;
		quosciente = (int) (resto / moeda);
		System.out.printf(quosciente +"moeda(s) de R$ %.2f%n", moeda);
		resto = resto % moeda;
		
		moeda = 0.50;
		quosciente = (int) (resto / moeda);
		System.out.printf(quosciente +"moeda(s) de R$ %.2f%n", moeda);
		resto = resto % moeda;
		
		moeda = 0.25;
		quosciente = (int) (resto / moeda);
		System.out.println(quosciente +"moeda(s) de R$ "+ moeda);
		resto = resto % moeda;
		
		moeda = 0.10;
		quosciente = (int) (resto / moeda);
		System.out.printf(quosciente +"moeda(s) de R$ %.2f%n", moeda);
		resto = resto % moeda;
		
		moeda = 0.05;
		quosciente = (int) (resto / moeda);
		System.out.println(quosciente +"moeda(s) de R$ "+ moeda);
		resto = resto %  moeda;
		
		moeda = 0.01;
		quosciente = (int) (resto / moeda);		
		System.out.println(quosciente +"moeda(s) de R$ "+ moeda);
	}
}
