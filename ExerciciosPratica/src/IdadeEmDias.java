import java.util.Scanner;
public class IdadeEmDias {
	public static void main(String atg[]) {
		Scanner sc = new Scanner(System.in);
		
		int dias = sc.nextInt();
		
		int cont = dias;
		
		int num = 365;
		int total = cont / num;
		System.out.println(total +" ano (s)");
		cont %= num;
		
		num = 30;
		total = cont / num;
		System.out.println(total +" mes (es)");
		cont %= num;
		
		System.out.println(cont +" dia (s)");
		
		sc.close();
		
	}
}
