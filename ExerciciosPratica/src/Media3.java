import java.util.Scanner;

public class Media3{
	public static void main(String arg[]){
		Scanner sc = new Scanner(System.in);
		
		float N1 = sc.nextFloat();
		float N2 = sc.nextFloat();
		float N3 = sc.nextFloat();
		float N4 = sc.nextFloat();
		
		double media = (N1 * 2 + N2 * 3 + N3 * 4 + N4 * 1)/10;
		
		System.out.printf("Media: %.1f%n", media);
		
		if(media >= 7.0) {
			System.out.println("Aluno Aprovado.");
		}
		else if(media < 5.0){
			System.out.println("Aluno Reprovado.");
		}
		else {
			System.out.println("Aluno em exame.");
			double NE = sc.nextFloat();
			NE += media / 2;
			
			if(NE > 5.0){
				System.out.println("Aluno Aprovado.");
			}
			else {
				System.out.println("Aluno Reprovado.");
			}
			System.out.printf("Media final: %.1f%n", NE);
		}
	}
}