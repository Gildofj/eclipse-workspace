import java.util.Scanner;
public class FormulaDeBhaskara {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		
		float A = sc.nextFloat();
		float B = sc.nextFloat();
		float C = sc.nextFloat();
		
		double tri = (Math.pow(B, 2.0)) - 4 * A * C;
		double x1 = (-B + Math.sqrt(tri))/(2*A);
		double x2 = (-B - Math.sqrt(tri))/(2*A);
		
		if(A > 0 && tri >= 0) {
			System.out.printf("R1 = %.5f%n", x1);
			System.out.printf("R2 = %.5f%n", x2);			
		} else {
			System.out.println("Impossivel calcular");
		}
		
		sc.close();
	}
}
