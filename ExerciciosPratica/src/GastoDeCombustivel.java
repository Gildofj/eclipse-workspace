import java.util.Scanner;
public class GastoDeCombustivel {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		
		int tempo = sc.nextInt();
		int velo = sc.nextInt();
		
		double total = ((velo * tempo) / 12.0);
		
		System.out.printf("%.3f%n", total);
		
		sc.close();
		}
}
