import java.util.Scanner;
import java.util.Locale;
public class Consumo {
	public static void main(String arg[]) {
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		int dist = sc.nextInt();
		float litros = sc.nextFloat();
		
		double kml = dist/litros;
		
		System.out.printf("%.3f km/l%n", kml);
		
		sc.close();
	}
}