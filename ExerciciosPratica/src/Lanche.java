import java.util.Scanner;
public class Lanche {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		
		int code = sc.nextInt();
		int quantidade = sc.nextInt();
		double total;
		
		if(code == 1) {
			total = quantidade * 4.00;
			System.out.printf("Total: R$ %.2f%n", total);
		} 
		else if(code == 2) {
			total = quantidade * 4.50;
			System.out.printf("Total: R$ %.2f%n", total);
		}
		else if(code == 3) {
			total = quantidade * 5.00;
			System.out.printf("Total: R$ %.2f%n", total);
		}
		else if(code == 4) {
			total = quantidade * 2.00;
			System.out.printf("Total: R$ %.2f%n", total);
		}
		else if(code == 5) {
			total = quantidade * 1.50;
			System.out.printf("Total: R$ %.2f%n", total);
		} 
		
	}
}
