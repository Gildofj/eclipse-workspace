import java.util.Scanner;
public class SalarioBonus {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		
		String nome = sc.nextLine();
		double salarioFixo = sc.nextDouble();
		double vendas = sc.nextDouble();
		
		double salario = salarioFixo + (vendas*0.15);
		
		System.out.printf("TOTAL = R$ %.2f%n", salario);
		
		sc.close();
	}
}
