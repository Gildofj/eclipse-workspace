import java.util.Scanner;
public class Esfera {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		double pi = 3.14159;
		
		float R = sc.nextFloat();
		
		double volume = (4/3.0) * pi * R * R * R;
		
		System.out.printf("VOLUME = %.3f%n", volume);
		
		sc.close();
	}
}
