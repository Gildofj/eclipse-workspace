import java.util.Scanner;
public class Distancia {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		
		int X = sc.nextInt();
		int Y = sc.nextInt();
		int dist = sc.nextInt();
		
		int min = (dist/Math.abs(Y - X))*60;
		
		System.out.println(min +" minutos");
		
		sc.close();
	}
}
