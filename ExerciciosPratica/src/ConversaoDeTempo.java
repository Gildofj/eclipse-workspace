import java.util.Scanner;
public class ConversaoDeTempo {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		
		int N = sc.nextInt();
		
		int horas = N / 3600;
		int resto = N % 3600;
		
		int minutos = resto / 60;
		int segundos = resto % 60;
		
		String hms = String.format("%02d:%02d:%02d", horas, minutos, segundos);
		
		System.out.println(hms);
		
		sc.close();
	}
}
