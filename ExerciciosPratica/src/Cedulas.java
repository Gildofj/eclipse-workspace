import java.util.Scanner;
public class Cedulas {
	public static void main(String arg[]) {
		Scanner sc = new Scanner(System.in);
		
		int N = sc.nextInt();
		int resto = N;
		
		int nota = 100;
		int quosciente = resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 50;
		quosciente = resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 20;
		quosciente = resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 10;
		quosciente = resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 5;
		quosciente = resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		nota = 2;
		quosciente = resto / nota;
		System.out.println(quosciente +"nota(s) de R$ "+ nota +",00");
		resto = resto % nota;
		
		System.out.println(resto +"nota(s) de R$ 1,00");
		
		
		sc.close();
	}
}
