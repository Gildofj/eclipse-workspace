import java.util.Scanner;
import java.text.DecimalFormat;
public class CalculaArea {
	public static void main(String arg[]) {
		double pi = 3.14159, raio;
		DecimalFormat df = new DecimalFormat("0.####");
		Scanner in = new Scanner(System.in);
		
		System.out.println("Insira o valor do raio da circuferencia para calculo da Area: ");
		raio = in.nextDouble();
		
		double A = pi * Math.pow(raio, 2);
		
		System.out.println("A="+ df.format(A));
	}
}
