import java.util.Scanner;
public class ifCompostoExemplo {
	private static int idade;
	public static void main(String arg[]) {
		//Cria��o de Scanner
		Scanner in = new Scanner(System.in);
		System.out.println("Insira a Idade");
		idade = in.nextInt();  //Input da vari�vel idade atrav�s do Scanner
		
		//ifComposto
		if (idade >= 65) {
			System.out.println("Melhor Idade");
		} else {
			System.out.println("Idade Complicada");
		}
	}
}