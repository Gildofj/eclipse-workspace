import java.util.*;
public class TesteArray4 {
	public static void main(String arg[]) {
		int idx[] = {2, 1, 9, 6, 4};
		
		System.out.println("Valore de um array para ORDENAR/CLASSIFICAR:");
		for(int valor : idx) {
			System.out.print(valor + "\t");
		}
		
		Arrays.sort(idx);
		
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Valores de um array ORDENADO/CLASSIFICADO:");
		for(int valor : idx) {
			System.out.print(valor + "\t");
		}
		
		System.out.println(" ");
		System.out.println(" ");
	}
}
