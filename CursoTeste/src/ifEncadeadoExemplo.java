import java.util.Scanner;
public class ifEncadeadoExemplo {
	private static int limitecheque;
	private static int saldomedia;
	
	public static void main(String args[]) {
		//Cria��o do Scanner
		Scanner in = new Scanner(System.in);
		
		System.out.println("Insira uma media do seu saldo: ");
		saldomedia = in.nextInt(); //Input da vari�vel saldomedia atrav�s do Scanner
		
		//ifEncadeado
		if(saldomedia > 5000) {
			limitecheque = 1000; 
		}else if(saldomedia > 3000 && saldomedia <= 5000) {
			limitecheque = 800;
		}else if(saldomedia > 2000 && saldomedia <= 3000) {
			limitecheque = 600;
		}else if(saldomedia > 1000 && saldomedia <= 2000) {
			limitecheque = 400;
		}else {
			limitecheque = 0;
		}
		System.out.println("O limite do seu cheque � de: "+ limitecheque);
	}
	

}
