
public class TesteArray3 {
	public static void main(String[] arg) {
		char[] NOMECURSO = {'S', 'I', 'S', 'T', 'E', 'M', 'A'};
		
		char[] CODECURSO = new char[4];
		
		System.arraycopy(NOMECURSO, 0, CODECURSO, 0, 4);
		
		System.out.println(new String(CODECURSO));
	}
}
