import java.util.Scanner;
public class SwitchBreakExemplo2 {
	
	public static void main(String arg[]) {
		int valor;
		String resultado = "";
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Insira o seu resultado de km/1: ");
		valor = in.nextInt();
		if(valor <= 12) {
		switch(valor) {
		
		case 12:
			resultado = "Excelente";
			break;
		case 11:
			resultado = "Muito Bom";
			break;
		case 10:
			resultado = "Bom";
			break;
		case 9:
			resultado = "Satisfatorio";
			break;
		case 8:
			resultado = "Baixo";
			break;
		case 7:
			resultado = "Muito Baixo";
			break;
		default:
			resultado = "Insatisfatorio";
			break;
		}
		
			System.out.println(resultado);
		} else {
			System.out.println("Acima do Esperado");
		}
		
	}

}
