
public class ProgString {
		public static void main(String arg[]) {
			System.out.println("\n Exemplo simples de String");
			System.out.println("\n=========================================");
			String v1 = "System";
			String v2 = new String("system");
			
			//equals
			System.out.println("Exemplo com metodo de comparacao: equals");
			if(v1.equals(v2)) {
				System.out.println("Variaveis identicas =>"+" v1 "+ v1+" v2 "+ v2);
			}else {
				System.out.println("Variaveis diferentes =>"+" v1 "+ v1+" v2 "+ v2);
			} 
			System.out.println("\n===========================================");
			//equalsIgnoreCase
			System.out.println("Exemplo sem o metodo de comparacao: equalsIgnoreCase");
			if(v1.equalsIgnoreCase(v2)) {
				System.out.println("Variaveis identicas =>"+" v1 "+ v1+" v2 "+ v2);
			}else {
				System.out.println("Variaveis diferentes =>"+" v1 "+ v1+" v2 "+ v2);
			}
			System.out.println("\n============================================");
			//Alguns m�todos
			//indexOf, charAt, substring
			System.out.println("Exemplos com metodos: indexOf, charAt, substring");
			String frase = "Desenvolvimento de sistemas";
			System.out.println("\nFrase-Exemplo => Desenvolvimento de sistemas");
			System.out.println("\nPosicao da fonte <a> na Frase-Exemplo (0 a 26 e com os espacos): "+ frase.charAt(5));
			
			System.out.println("==============================================");
			//concat
			String titulo1 = "Desenvolvimento", titulo2 = " de sistemas";
			String CursoCompleto = titulo1.concat(titulo2);
			System.out.println("\ntitulo 1:"+ titulo1);
			System.out.println("\ntitulo 2:"+ titulo2);
			System.out.println("\nExemplo Concatenado => titulo 1 + titulo 2: "+ CursoCompleto);
			
		}
}
