
public class TestCasting2 {
	public static void main(String arg[]) {
		//Exemplo1
		byte a = 1;
		byte b = 3;
		/* Linha errada pois a adi��o sofre casting implicita */
		//byte c = a + b;
		//Corre��o do casting explicito 
		byte c = (byte) (a+b);
		System.out.println(c);
		//Exemplo2
		int ia = 1;
		long ib = 3;
		//Perda de Precis�o - causa erro 
		//int ic =  ia + ib; //linha errada => Casting 
		//Corre��o o casting explicito
		int ic = ia + (int) ib;
		System.out.println(ic);
	}
}
