import java.util.Scanner;
public class SwitchBreakExemplo {
	
	public static void main(String arg[]) {
		char indice;
		String resultado = "";
		//Cria��o do Scanner
		Scanner in = new Scanner(System.in);
		
		System.out.println("Insira uma letra de 'a' a 'c': ");
		indice = in.next().charAt(0);  //Input da variavel indice atrav�s don Scanner
		
		switch (indice) {
		
		case 'a':
			resultado += 'a';
			break;
		case 'b':
			resultado += 'b';
			break;
		case 'c':
			resultado += 'c';
			break;
		default:
			resultado += 'd';
			break;
			
		}
		System.out.println(resultado);
	}
}
