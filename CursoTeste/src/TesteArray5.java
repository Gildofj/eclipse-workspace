import java.util.Arrays;
public class TesteArray5 {
	public static void main(String arg[]) {
		
		String idx[] = {"A", "E", "C", "B", "D"};
		
		System.out.println("Valore de um array para ORDENAR/CLASSIFICAR:");
		for(String valor : idx) {
			System.out.print(valor + "\t");
		}
		
		Arrays.sort(idx);
		
		System.out.println(" ");
		System.out.println(" ");
		
		System.out.println("Valores de um array ORDENADO/CLASSIFICADO:");
		for(String valor : idx) {
			System.out.print(valor + "\t");
		}
		
		System.out.println(" ");
		System.out.println(" ");
	}
}
