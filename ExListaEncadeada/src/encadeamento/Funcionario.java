package encadeamento;

public class Funcionario {
	String nome;
	String salario;
	
	public String getNome () {
		return nome;
	}
	public void setNome (String nome) {
		this.nome = nome;
	}
	public String getSalario () {
		return salario;
	}
	public void setSalario (String salario) {
		this.salario = salario;
	}
}
