
public class CadastroAtendente {
private String nome, endereco, cpf;

		public CadastroAtendente() {
			nome="";
			endereco="";
			cpf="";
		}
		public void alteraNome (String snome) {
			nome=snome;
		}
		public void alteraEndereco (String sendereco) {
			endereco=sendereco;
		}
		public void alteraCpf (String scpf) {
			cpf=scpf;
		}
		public String forneceNome() {
			return nome;
		}
		public String forneceEndereco() {
			return endereco;
		}
		public String forneceCpf() {
			return cpf;
		}
}
