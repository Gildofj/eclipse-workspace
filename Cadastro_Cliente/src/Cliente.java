/*Todo m�todo � definido com a palavra PUBLIC e todo atributo com a palavra PRIVATE.
 * A defini��o inicial da classe come�a com PUBLIC
 * CLASSE Cliente sendo criada com os atributos gerai*/
public class Cliente {
/*Atributos da Classe Cliente definidos com o tipo STRING*/
	private String nome, endereco, cpf;
/*M�todo construtor que somente � executado no momento em que um objetivo � criado(instanciado)*/
	public Cliente () {
		/*inicializando os atributos*/
		nome="";
		endereco="";
		cpf="";
	}
	/*instancia atrbutos da classe cliente*/
	public void alteraNome (String snome) {
		nome=snome;
	}
	public void alteraEndereco (String sendereco) {
		endereco=sendereco;
	}
	public void alteraCpf (String scpf) {
		cpf=scpf;
	}
	/*retorna atributos instanciados para a impress�o em tela*/
	public String forneceNome() {
		return nome;
	}
	public String forneceEndereco() {
		return endereco;
	}
	public String forneceCpf() {
		return cpf;
	}
	}
