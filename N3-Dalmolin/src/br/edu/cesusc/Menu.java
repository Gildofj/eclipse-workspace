package br.edu.cesusc;

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Menu {

  private static Scanner entrada;

  public static void main(String[] args) throws IOException {
    
    entrada = new Scanner(System.in);

    // Variável do tamanho do array
    int tamanhoArray = 0;
    // Variável do tamanho máximo do valor random do array
    int tamanhoMaximoArray;

    // Carregando array

    System.out.println("Digite o TAMANHO do array ->");
    tamanhoArray = entrada.nextInt();

    System.out.println("Digite o VALOR MAXIMO do array ->");
    tamanhoMaximoArray = entrada.nextInt();

    // array
    int[] array = new int[tamanhoArray];

    Random gerador = new Random();

    for (int i = 0; i < tamanhoArray; i++) {
      array[i] = gerador.nextInt(tamanhoMaximoArray);
    }
    
    System.out.println("1 Listar");
    System.out.println("2 Selec�o");
    System.out.println("3 Inser��o");
    System.out.println("4 Shelsort");
    System.out.println("5 Quicksort");
    System.out.println("6 Heapsort");
    System.out.println("7 sair");

    int opc = entrada.nextInt();
    while (opc < 9) {
      switch (opc) {
      case 1: // CASE 01 listando array
        
        long tempoInicial = System.currentTimeMillis();

        for (int i = 0; i < tamanhoArray; i++) {
          System.out.println("Na posis�o " + i + " o valor � " + array[i]);
        }
        
        long tempoFinal = System.currentTimeMillis();
        
        System.out.println("\nExecutado em = " + (tempoFinal - tempoInicial) + " ms");
        
        break;

      case 2: // CASE 02 Ordenando com o método seleção

        long tempoInicial1 = System.currentTimeMillis();

        selecao(array);

        for (int i = 0; i < tamanhoArray; i++) {
          System.out.println("Na posi��o " + i + " o valor � " + array[i]);
        }

        long tempoFinal1 = System.currentTimeMillis();

        System.out.println("\nExecutado em = " + (tempoFinal1 - tempoInicial1) + " ms");

        break;

      case 3: // CASE 03 Ordenando com o método inserção

        long tempoInicial2 = System.currentTimeMillis();

        insercao(array);

        for (int i = 0; i < tamanhoArray; i++) {
          System.out.println("Na posi��o " + i + " o valor � " + array[i]);
        }

        long tempoFinal2 = System.currentTimeMillis();

        System.out.println("\nExecutado em = " + (tempoFinal2 - tempoInicial2) + " ms");

        break;

      case 4: // CASE 04 Ordenando com o método shellsort

        long tempoInicial3 = System.currentTimeMillis();

        shellsort(array);

        for (int i = 0; i < tamanhoArray; i++) {
          System.out.println("Na posi��o " + i + " o valor � " + array[i]);
        }

        long tempoFinal3 = System.currentTimeMillis();

        System.out.println("Executado em = " + (tempoFinal3 - tempoInicial3) + " ms");

        break;

      case 5: // CASE 05 Ordenando com o método quicksort

        long tempoInicial4 = System.currentTimeMillis();

        quicksort(array, 0, array.length - 1);

        for (int i = 0; i < tamanhoArray; i++) {
          System.out.println("Na posi��o " + i + " o valor � " + array[i]);
        }

        long tempoFinal4 = System.currentTimeMillis();

        System.out.println("Executado em = " + (tempoFinal4 - tempoInicial4) + " ms");

        break;

      case 6: // CASE 06 Ordenando com o método heapSort

        long tempoInicial5 = System.currentTimeMillis();

        heapsort(array);

        for (int i = 0; i < tamanhoArray; i++) {
          System.out.println("Na posi��o " + i + " o valor � " + array[i]);
        }

        long tempoFinal5 = System.currentTimeMillis();

        System.out.println("Executado em = " + (tempoFinal5 - tempoInicial5) + " ms");

        break;

      case 7: // CASE 07 - Encerrar Programa
        System.out.println("Programa Encerrado!!!");
        break;

      }
      opc = entrada.nextInt();
    }
  }
  
  public static void selecao(int[] arraySS) {
    for (int fixo = 0; fixo < arraySS.length - 1; fixo++) {
      int menor = fixo;

      for (int i = menor + 1; i < arraySS.length; i++) {
        if (arraySS[i] < arraySS[menor]) {
          menor = i;
        }
      }
      if (menor != fixo) {
        int t = arraySS[fixo];
        arraySS[fixo] = arraySS[menor];
        arraySS[menor] = t;
      }
    }
  }

  public static void insercao(int[] arrayIS) {
    int i;
    int j;
    int key;

    for (j = 1; j < arrayIS.length; j++) {
      key = arrayIS[j];
      for (i = j - 1; (i >= 0) && (arrayIS[i] > key); i--) {
        arrayIS[i + 1] = arrayIS[i];
      }
      arrayIS[i + 1] = key;
    }
  }

  public static void shellsort(int[] arraySH) {
    int h = 1;
    int n = arraySH.length;

    while (h < n) {
      h = h * 3 + 1;
    }

    h = h / 3;
    int c, j;

    while (h > 0) {
      for (int i = h; i < n; i++) {
        c = arraySH[i];
        j = i;
        while (j >= h && arraySH[j - h] > c) {
          arraySH[j] = arraySH[j - h];
          j = j - h;
        }
        arraySH[j] = c;
      }
      h = h / 2;
    }
  }

  static void quicksort(int[] arrayQS, int inicio, int fim) {
    if (inicio < fim) {
      int posicaoPivo = separar(arrayQS, inicio, fim);
      quicksort(arrayQS, inicio, posicaoPivo - 1);
      quicksort(arrayQS, posicaoPivo + 1, fim);
    }
  }

  private static int separar(int[] arrayQS, int inicio, int fim) {
    int pivo = arrayQS[inicio];
    int i = inicio + 1, f = fim;
    while (i <= f) {
      if (arrayQS[i] <= pivo)
        i++;
      else if (pivo < arrayQS[f])
        f--;
      else {
        int troca = arrayQS[i];
        arrayQS[i] = arrayQS[f];
        arrayQS[f] = troca;
        i++;
        f--;
      }
    }
    arrayQS[inicio] = arrayQS[f];
    arrayQS[f] = pivo;
    return f;
  }

  public static void heapsort(int[] arrayHS) {
    int n = arrayHS.length;

    for (int i = n / 2 - 1; i >= 0; i--)
      amontoar(arrayHS, n, i);

    for (int i = n - 1; i >= 0; i--) {

      int auxil = arrayHS[0];
      arrayHS[0] = arrayHS[i];
      arrayHS[i] = auxil;

      amontoar(arrayHS, i, 0);
    }
  }

  static void amontoar(int[] arrayHS, int n, int i) {
    int maior = i;
    int op1 = 2 * i + 1;
    int op2 = 2 * i + 2;

    if (op1 < n && arrayHS[op1] > arrayHS[maior])
      maior = op1;

    if (op2 < n && arrayHS[op2] > arrayHS[maior])
      maior = op2;

    if (maior != i) {
      int troca = arrayHS[i];
      arrayHS[i] = arrayHS[maior];
      arrayHS[maior] = troca;

      amontoar(arrayHS, n, maior);
    }
  }

  
  
}
